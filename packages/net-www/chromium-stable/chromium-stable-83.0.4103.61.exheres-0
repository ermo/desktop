# Copyright 2009-2011 Elias Pipping <pipping@exherbo.org>
# Copyright 2009 Heiko Przybyl <zuxez@cs.tu-berlin.de>
# Distributed under the terms of the GNU General Public License v2

require chromium

DESCRIPTION+="
Chromium has three release channels: Stable, Beta, and Dev. This package corresponds to the Stable
channel.
"

PLATFORMS="~amd64 ~x86"

RESTRICT="test"

DEPENDENCIES+="
    build+run:
        !net-www/chromium-stable-flash-plugin [[
            description = [ Chrome binary plugins don't contain flash anymore ]
            resolution = uninstall-blocked-before
        ]]
        !net-www/chromium-stable-pdf-plugin [[
            description = [ Chromium now provides libpdf ]
            resolution = uninstall-blocked-before
        ]]
    suggestion:
        net-www/chromium-stable-widevine-plugin [[ description = [ Content Decryption Module plugin required for e.g. Netflix ] ]]
"

# Overview with URLs for distro-specific patches
# https://chromium.googlesource.com/chromium/src/+/master/docs/linux_chromium_packages.md
DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/chromium-webrtc-r0.patch
    "${FILES}"/chromium-swiftshader-default-visibility.patch
    "${FILES}"/chromium-widevine.patch
    "${FILES}"/chromium-remove-no-canonical-prefixes.patch
    "${FILES}"/chromium-gl_defines_fix.patch
    "${FILES}"/chromium-unbundle-zlib.patch
    "${FILES}"/chromium-base-unittests-icu-fix.patch
    "${FILES}"/chromium-font-missing-include.patch
    "${FILES}"/chromium-libstdc++-fix-incomplete-type.patch
    "${FILES}"/chromium-add-missing-include-find.patch
    "${FILES}"/chromium-add-missing-include-numeric_limits.patch
    "${FILES}"/chromium-add-missing-include-unique_ptr.patch
    "${FILES}"/chromium-libstdc++-fix-iterator-any_of.patch
    "${FILES}"/chromium-libstdc++-fix-iterator-all_of.patch
    "${FILES}"/chromium-fix-re2-set_utf8.patch
)

CHROMIUM_KEEPLIBS=(
    base/third_party/cityhash
    base/third_party/double_conversion/double-conversion
    base/third_party/dynamic_annotations
    base/third_party/icu
    base/third_party/nspr
    base/third_party/superfasthash
    base/third_party/symbolize
    base/third_party/valgrind
    base/third_party/xdg_mime
    base/third_party/xdg_user_dirs
    buildtools/third_party/libc++
    buildtools/third_party/libc++abi
    chrome/third_party/mozilla_security_manager
    courgette/third_party
    net/third_party/mozilla_security_manager
    net/third_party/nss
    net/third_party/quiche
    net/third_party/uri_template
    third_party/abseil-cpp
    third_party/angle
    third_party/angle/src/common/third_party/base
    third_party/angle/src/common/third_party/smhasher
    third_party/angle/src/common/third_party/xxhash
    third_party/angle/src/third_party/compiler
    third_party/angle/src/third_party/libXNVCtrl
    third_party/angle/src/third_party/trace_event
    third_party/angle/src/third_party/volk
    third_party/angle/third_party/glslang
    third_party/angle/third_party/spirv-headers
    third_party/angle/third_party/spirv-tools
    third_party/angle/third_party/vulkan-headers
    third_party/angle/third_party/vulkan-loader
    third_party/angle/third_party/vulkan-tools
    third_party/angle/third_party/vulkan-validation-layers
    third_party/apple_apsl
    third_party/axe-core
    third_party/blink
    third_party/boringssl
    third_party/boringssl/src/third_party/fiat
    third_party/breakpad
    third_party/breakpad/breakpad/src/third_party/curl
    third_party/brotli
    third_party/cacheinvalidation
    third_party/catapult
    third_party/catapult/common/py_vulcanize/third_party/rcssmin
    third_party/catapult/common/py_vulcanize/third_party/rjsmin
    third_party/catapult/third_party/polymer
    third_party/catapult/tracing/third_party/d3
    third_party/catapult/tracing/third_party/gl-matrix
    third_party/catapult/tracing/third_party/jpeg-js
    third_party/catapult/tracing/third_party/jszip
    third_party/catapult/tracing/third_party/mannwhitneyu
    third_party/catapult/tracing/third_party/oboe
    third_party/catapult/tracing/third_party/pako
    third_party/ced
    third_party/cld_3
    third_party/closure_compiler
    third_party/crashpad
    third_party/crashpad/crashpad/third_party/lss
    third_party/crashpad/crashpad/third_party/zlib
    third_party/crc32c
    third_party/cros_system_api
    third_party/dav1d
    third_party/dawn
    third_party/depot_tools
    third_party/devscripts
    third_party/devtools-frontend/
    third_party/devtools-frontend/src/front_end/third_party/fabricjs
    third_party/devtools-frontend/src/front_end/third_party/lighthouse
    third_party/devtools-frontend/src/front_end/third_party/wasmparser
    third_party/devtools-frontend/src/third_party/axe-core
    third_party/devtools-frontend/src/third_party/pyjson5/src/json5
    third_party/devtools-frontend/src/third_party/typescript
    third_party/dom_distiller_js
    third_party/emoji-segmenter
    third_party/flatbuffers
    third_party/freetype
    third_party/google_input_tools
    third_party/google_input_tools/third_party/closure_library
    third_party/google_input_tools/third_party/closure_library/third_party/closure
    third_party/googletest
    third_party/glslang
    third_party/harfbuzz-ng/utils
    third_party/hunspell
    third_party/iccjpeg
    third_party/icu
    third_party/inspector_protocol
    third_party/jsoncpp
    third_party/jinja2
    third_party/jstemplate
    third_party/khronos
    third_party/leveldatabase
    third_party/libXNVCtrl
    third_party/libaddressinput
    third_party/libaom
    third_party/libaom/source/libaom/third_party/vector
    third_party/libaom/source/libaom/third_party/x86inc
    third_party/libgifcodec
    third_party/libjingle
    third_party/libphonenumber
    third_party/libsecret
    third_party/libsrtp
    third_party/libsync
    third_party/libudev
    third_party/libwebm
    third_party/libxml
    third_party/libxml/chromium
    third_party/libyuv
    third_party/llvm
    third_party/lss
    third_party/lzma_sdk
    third_party/mako
    third_party/markupsafe
    third_party/metrics_proto
    third_party/modp_b64
    third_party/nasm
    third_party/node
    third_party/node/node_modules/polymer-bundler/lib/third_party/UglifyJS2
    third_party/one_euro_filter
    third_party/openscreen
    third_party/openscreen/src/third_party/tinycbor
    third_party/ots
    third_party/perfetto
    third_party/pdfium
    third_party/pdfium/third_party/agg23
    third_party/pdfium/third_party/base
    third_party/pdfium/third_party/bigint
    third_party/pdfium/third_party/freetype
    third_party/pdfium/third_party/lcms
    third_party/pdfium/third_party/libopenjpeg20
    third_party/pdfium/third_party/libpng16
    third_party/pdfium/third_party/libtiff
    third_party/pdfium/third_party/skia_shared
    third_party/pffft
    third_party/ply
    third_party/polymer
    third_party/private-join-and-compute
    third_party/protobuf
    third_party/protobuf/third_party/six
    third_party/pyjson5/src/json5
    third_party/qcms
    third_party/rnnoise
    third_party/s2cellid
    third_party/schema_org
    third_party/skia
    third_party/skia/include/third_party/skcms
    third_party/skia/include/third_party/vulkan/vulkan
    third_party/skia/third_party/skcms
    third_party/skia/third_party/vulkanmemoryallocator
    third_party/smhasher
    third_party/spirv-headers
    third_party/SPIRV-Tools
    third_party/sqlite
    third_party/swiftshader
    third_party/swiftshader/third_party/astc-encoder
    third_party/swiftshader/third_party/llvm-7.0
    third_party/swiftshader/third_party/llvm-subzero
    third_party/swiftshader/third_party/marl
    third_party/swiftshader/third_party/SPIRV-Headers
    third_party/swiftshader/third_party/subzero
    third_party/tcmalloc
    third_party/unrar
    third_party/usrsctp
    third_party/vulkan
    third_party/web-animations-js
    third_party/webdriver
    third_party/webrtc
    third_party/webrtc/common_audio/third_party/fft4g
    third_party/webrtc/common_audio/third_party/spl_sqrt_floor
    third_party/webrtc/modules/third_party/fft
    third_party/webrtc/modules/third_party/g711
    third_party/webrtc/modules/third_party/g722
    third_party/webrtc/rtc_base/third_party/base64
    third_party/webrtc/rtc_base/third_party/sigslot
    third_party/widevine
    third_party/woff2
    third_party/zlib/google
    url/third_party/mozilla
    v8/src/third_party/siphash
    v8/src/third_party/utf8-decoder
    v8/src/third_party/valgrind
    v8/third_party/inspector_protocol
    v8/third_party/v8
)

# TODO: keep for GN builds
CHROMIUM_KEEPLIBS+=(
    third_party/adobe
    third_party/speech-dispatcher
    third_party/usb_ids
    third_party/xdg-utils
    third_party/yasm/run_yasm.py
    tools/gn/src/base/third_party/icu
)

# vp9_impl.cc:720:25: error: 'const struct vpx_codec_cx_pkt::<unnamed union>::<unnamed>' has no member named 'height'
CHROMIUM_KEEPLIBS+=(
    third_party/libvpx
    third_party/libvpx/source/libvpx/third_party/x86inc
)

# libvpx: see keeplibs above
CHROMIUM_SYSTEM_LIBS="
    ffmpeg
    flac
    fontconfig
    libdrm
    libevent
    libjpeg
    libpng
    libwebp
    libxml
    libxslt
    openh264
    opus
    re2
    snappy
    yasm
    zlib
"

src_install() {
    chromium_src_install

    # Required because we don't use system icu
    insinto /opt/${MY_PN}
    doins icudtl.dat
}

