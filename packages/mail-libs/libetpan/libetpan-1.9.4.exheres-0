# Copyright 2008, 2009, 2010 Mike Kelly
# Distributed under the terms of the GNU General Public License v2

require github [ user=dinhviethoa ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
require option-renames [ renames=[ 'gnutls providers:gnutls' ] ]

SUMMARY="An email client library, for IMAP, POP, etc."
DESCRIPTION="
The purpose of this mail library is to provide a portable, efficient framework for different kinds
of mail access. When using the drivers interface, the interface is the same for all kinds of mail
access, remote and local mailboxes.
"
HOMEPAGE+=" https://www.etpan.org/${PN}.html"

BUGS_TO="pioto@exherbo.org"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    berkdb
    lockfile [[ description = [ Use liblockfile for locking mailboxes ] ]]
    sasl
    ( providers: gnutls libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/expat [[ note = [ optional ] ]]
        net-misc/curl [[ note = [ optional ] ]]
        sys-libs/zlib[>=1.2.0.4] [[ note = [ optional ] ]]
        berkdb? ( sys-libs/db:=[>3&<6.1] )
        lockfile? ( net-libs/liblockfile )
        providers:gnutls? (
            dev-libs/gnutls
            dev-libs/libgcrypt
            dev-libs/libgpg-error
        )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        sasl? ( net-libs/cyrus-sasl )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-ipv6
    --enable-threads
    --disable-lmdb
    --disable-static
    --with-poll
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'berkdb db'
     lockfile
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'providers:gnutls gnutls'
    'providers:libressl openssl'
    'providers:openssl openssl'
    sasl
)

