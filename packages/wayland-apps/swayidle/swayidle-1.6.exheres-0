# Copyright 2019 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user='swaywm' ] meson

SUMMARY="Idle management daemon for Wayland"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    bash-completion
    zsh-completion
    ( providers: elogind systemd ) [[
        *description = [ Provider of logind support library ]
        number-selected = at-most-one
    ]]
"

DEPENDENCIES="
    build:
        app-doc/scdoc
        sys-libs/wayland-protocols[>=1.14]
    build+run:
        sys-libs/wayland
        providers:systemd? (
            sys-apps/systemd
        )
        providers:elogind? (
            sys-auth/elogind
        )
"

BUGS_TO="danyspin97@protonmail.com"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dfish-completions=false
    # by default no logind library is used
    -Dlogind=disabled
    -Dman-pages=enabled
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'bash-completion bash-completions'
    'zsh-completion zsh-completions'
)

MESON_SRC_CONFIGURE_OPTIONS=(
    # override previous option (logind=disabled)
    # this works because the last option issued is
    # the one considered by meson
    'providers:elogind -Dlogind=enabled'
    'providers:elogind -Dlogind-provider=elogind'
    'providers:systemd -Dlogind=enabled'
    'providers:systemd -Dlogind-provider=systemd'
)

