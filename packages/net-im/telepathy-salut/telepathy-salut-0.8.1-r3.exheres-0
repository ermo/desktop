# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require python [ multibuild=false blacklist=3 ]

SUMMARY="Salut Plugin for Telepathy"
HOMEPAGE="https://telepathy.freedesktop.org/wiki/"
DOWNLOADS="https://telepathy.freedesktop.org/releases/${PN}/${PNV}.tar.gz"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gtk-doc
    ( providers: gnutls libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-libs/libxslt
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc )
    build+run:
        dev-libs/glib:2[>=2.28]
        sys-apps/dbus[>=1.1.0]
        dev-libs/dbus-glib:1[>=0.61]
        dev-libs/libxml2:2.0
        net-im/telepathy-glib[>=0.19.7]
        net-dns/avahi[dbus]
        gnome-desktop/libsoup:2.4
        providers:gnutls? ( dev-libs/gnutls )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
    test:
        dev-python/dbus-python
        net-libs/cyrus-sasl[>=2]
        net-twisted/Twisted
"

RESTRICT="test" # TODO: requires X

src_configure() {
    econf --disable-avahi-tests                         \
          --with-tls=$(optionv providers:gnutls || echo openssl)  \
          $(option_enable providers:gnutls prefer-stream-ciphers) \
          $(option_enable gtk-doc)
}

src_test() {
    unset DBUS_SESSION_BUS_ADDRESS

    esandbox allow_net "unix-abstract:/tmp/dbus-*"
    default
    esandbox disallow_net "unix-abstract:/tmp/dbus-*"
}
