# Copyright 2020 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

RESHADE_SHADERS_REV="aff8ef2f34d375f43245d8da2ae495a62f4c1052"

require github [ user=DadSchoorse project=vkBasalt release=v${PV} pnv=vkBasalt suffix=tar.gz ]

SUMMARY="Vulkan post processing layer to enhance the visual graphics"
DESCRIPTION="
Build in effects are:
* Contrast Adaptive Sharpening
* Fast Approximate Anti-Aliasing
* Enhanced Subpixel Morphological Anti-Aliasing
* Deband/Dithering
* 3D color LookUp Table
It is also possible to use Reshade Fx shaders.
"

# https://github.com/DadSchoorse/vkBasalt/issues/58
DOWNLOADS="
    https://github.com/DadSchoorse/vkBasalt/releases/download/v${PV}/vkBasalt.tar.gz -> vkBasalt-${PV}.tar.gz
    https://github.com/crosire/reshade-shaders/archive/${RESHADE_SHADERS_REV}.tar.gz -> reshade-shaders-${RESHADE_SHADERS_REV}.tar.gz
"

LICENCES="as-is"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEFAULT_SRC_INSTALL_PARAMS=( PREFIX=/usr )

DEPENDENCIES="
    build:
        dev-lang/glslang
        dev-lang/spirv-tools
        sys-libs/vulkan-headers
        sys-libs/vulkan-validation-layers
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-0.3.0-makefiles.patch
)

src_prepare() {
    default

    # install library to lib instead of share
    # https://github.com/DadSchoorse/vkBasalt/issues/44
    edo sed \
        -e "s:@lib:/usr/$(exhost --target)/lib/libvkbasalt.so:g" \
        -i config/vkBasalt64.json
    edo sed \
        -e "s:@lib:\$(DESTDIR)/usr/$(exhost --target)/lib:g" \
        -i src/makefile

    # enable ReShade support
    edo sed \
        -e 's:*path/to/reshade-shaders/Shaders\*:/usr/share/vkBasalt/reshade-shaders/Shaders:g' \
        -e 's:*path/to/reshade-shaders/Textures\*:/usr/share/vkBasalt/reshade-shaders/Textures:g' \
        -e 's:#reshade:reshade:g' \
        -i config/vkBasalt.conf
}

src_install() {
    default

    edo mv "${IMAGE}"/usr/share/vkBasalt/vkBasalt.conf{,.example}

    insinto /usr/share/vkBasalt/reshade-shaders
    doins -r "${WORKBASE}"/reshade-shaders-${RESHADE_SHADERS_REV}/*
}

